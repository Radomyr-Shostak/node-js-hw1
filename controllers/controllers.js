const fs = require('fs');
const path = require('path');


const createFile = (req, res) => {
  const { filename, content } = req.body;

  try {
    fs.promises.writeFile(`./files/${filename}`, `${content}`, 'utf-8');
    res.status(200).json({ message : 'File created successfully' })
  } catch {
    res.status(500).json({ message : 'Server error' });
  }
}

const getFiles = async (req, res) => {

  try{
    const filesList = await fs.promises.readdir('./files');
    res.status(200).json({
      message: 'Success',
      files: filesList
    })
  } catch {
    res.status(500).json({ message: 'Server error' })
  }
}

const getFile = async (req, res) => {
  const filename = req.params.filename;
  const fileExt = path.extname(filename);


  if(!fs.existsSync(`./files/${filename}`)){
    return res.status(400).json({ message: `No file with '${filename}' filename found` })
  }

  try{
    const { birthtime } = await fs.promises.stat(`./files/${filename}`);
    const content = await fs.promises.readFile(`./files/${filename}`, 'utf-8');
    res.status(200).json({
      message: 'Success',
      filename,
      content,
      extension : fileExt,
      uploadedDate : birthtime
    })
  } catch {
    res.status(500).json({ message: 'Server error' })
  }
}

module.exports = { createFile, getFiles, getFile };