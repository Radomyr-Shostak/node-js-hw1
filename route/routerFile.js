const express = require('express');
const router = express.Router();
const validateFile = require('../middleware/validator');
const { createFile, getFiles, getFile } = require('../controllers/controllers');

router.route('/').post(validateFile, createFile);
router.route('/:filename').get(getFile);
router.route('/').get(getFiles);


module.exports = router;