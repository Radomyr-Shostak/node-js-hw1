const path = require('path');

const validateFile = (req, res, next) => {
  const fileExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
  const { filename, content } = req.body;

  if (!filename || !filename.trim()) {
    return res.status(400).json({ message: "Please specify 'filename' parametr" })
  };

  if (fs.existsSync(`./files/${filename}`)) {
    return res.status(400).json({ message: `File already exists` })
  };
  
  if (!fileExtensions.includes(path.extname(filename))) {
      return res.status(400).json({ message: 'File is not allowed' });
  };

  if (!content) {
      return res.status(400).json({ message: "Please specify 'content' parametr" });
  };

  next();

}

module.exports = validateFile;